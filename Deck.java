import java.util.ArrayList;

public class Deck {
    private ArrayList<UnoCard> deck = new ArrayList<UnoCard>();

    public Deck() {
        addWildCardAnd4PickUp();
        for(int i=0; i<2; i++){
            addNumberedCards();
            addSpecialCards();
        }    
    }
    
    /**
     * This method returns the first card in the deck available to be drawn.
     * @return UnoCard
     */
    public UnoCard draw(){
        return deck.remove(0);
    }
    /**
     * Adds a card to the bottom of the deck.
     * @param card : Card that gets added to the deck
     */
    public void addToDeck(UnoCard card){
        deck.add(card);
    }

    /**
     * Creates Wild Cards nad WildCard4PickUp
     */
    private void addWildCardAnd4PickUp() {
        for(int i=0; i<4;i++){
            /* Wild Cards */
            this.deck.add(new WildCard());
             /* Wild PickUp4Cards */
            this.deck.add(new WildPickUp4Card());
        }      
    }

    /**
     * Creates all numberedCards [green,yellow,red,blue 0-9]
     */
    private void addNumberedCards() {
        for(NumberedCards.Number number : NumberedCards.Number.values()){
            for(UnoCard.Color color : UnoCard.Color.values()){
                this.deck.add(new NumberedCards(color, number));
            }
        }
    }   
    
    /**
     * Creates all Special Cards. [reverscard,skipcard,pickup2card]
     */
    private void addSpecialCards(){
        for(UnoCard.Color color : UnoCard.Color.values()){
            this.deck.add(new ReverseCard(color));
            this.deck.add(new SkipCard(color));
            this.deck.add(new PickUp2Card(color));
        }
    }   
    /**
     * This  method returns the deck.
     */
    public ArrayList<UnoCard> getDeck(){
        return this.deck;
    }
}
