public class SkipCard extends ColoredCards{

    public SkipCard(Color color){
        this.color = color;
    }

    public boolean canPlay(UnoCard card){
        if(this.color == card.getColor()){
            return true;
        }
        return false;
    }
}
