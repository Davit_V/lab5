public class PickUp2Card extends ColoredCards{

    public PickUp2Card(Color color){
        this.color = color;
    }

    public boolean canPlay(UnoCard card){
        if(this.color == card.getColor()){
            return true;
        }
        return false;
    }
    
}
