public class NumberedCards extends ColoredCards{
    private Number number;

    public enum Number{
        ZERO,ONE,TWO,THREE,FOUR,FIVE,SIX,SEVEN,EIGHT,NINE 
    }

    public NumberedCards(Color color,Number number){
        this.color = color;
        this.number = number;
    }

    public boolean canPlay(UnoCard playedCard){
        if( (this.color == (playedCard.getColor()))
        || (this.number == ((NumberedCards)playedCard).getNumber())
        ){
            return true;
        }
    return false;
    }

    public Number getNumber(){
        return this.number;
    }
}