public class ReverseCard extends ColoredCards {

    public ReverseCard(Color color){
        this.color = color;
    }

    public boolean canPlay(UnoCard card){
        if(this.color == card.getColor()){
            return true;
        }
        return false;
    }
    
}
