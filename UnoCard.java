public interface UnoCard{
    // public Color color=null;

    public enum Color{
        RED, BLUE, YELLOW, GREEN
    }

    Color getColor();

    boolean canPlay(UnoCard playedCard);

}
