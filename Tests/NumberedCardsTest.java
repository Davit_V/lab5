import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class NumberedCardsTest {
    
    @Test
    public void NumberConstructorTest(){
        NumberedCards num = new NumberedCards(UnoCard.Color.GREEN, NumberedCards.Number.ZERO);

        assertEquals(NumberedCards.Number.ZERO, num.getNumber());
        assertEquals(UnoCard.Color.GREEN, num.getColor());
    }

    @Test
    public void canPlayTest(){
        NumberedCards num = new NumberedCards(UnoCard.Color.YELLOW, NumberedCards.Number.TWO);
        //Color canPlay;
        UnoCard playedCard = new NumberedCards(UnoCard.Color.YELLOW, NumberedCards.Number.FIVE);
        assertEquals(true, num.canPlay(playedCard));
        //NUmber canplay;
        UnoCard playedCard2= new NumberedCards(UnoCard.Color.GREEN, NumberedCards.Number.TWO);
        assertEquals(true, num.canPlay(playedCard2));
    }

}
