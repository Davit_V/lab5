import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class PickUp2CardTest {
    @Test
    public void PickUp2CanPlayTest(){
        PickUp2Card card = new PickUp2Card(UnoCard.Color.BLUE);
        UnoCard played = new ReverseCard(UnoCard.Color.BLUE);
        assertEquals(true,card.canPlay(played));
    }

}
