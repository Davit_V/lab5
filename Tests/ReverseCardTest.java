import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class ReverseCardTest {
    @Test
    public void ReverseCanPlayTest(){
        ReverseCard card = new ReverseCard(UnoCard.Color.GREEN);
        UnoCard played = new ReverseCard(UnoCard.Color.GREEN);
        assertEquals(true,card.canPlay(played));
    }

}