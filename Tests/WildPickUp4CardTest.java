import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class WildPickUp4CardTest {
    @Test
    public void Wild4CanPlayTest(){
        NumberedCards card = new NumberedCards(UnoCard.Color.YELLOW,NumberedCards.Number.NINE);
        WildPickUp4Card wild = new WildPickUp4Card();
        //card  color is  set to null in UnoCard.java
        assertEquals(null, wild.getColor());
        //Playable
        assertEquals(true,wild.canPlay(card));
        //Card Color is set
        assertEquals(UnoCard.Color.YELLOW, wild.getColor());
    }

    @Test
    public void Wild4getColorTest(){
        WildPickUp4Card card = new WildPickUp4Card();
        card.setColor(UnoCard.Color.GREEN);
        
        assertEquals(UnoCard.Color.GREEN, card.getColor());
    }
}
