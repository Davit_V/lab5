import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class SkipCardTest {
    @Test
    public void SkipCanPlayTest(){
        SkipCard card = new SkipCard(UnoCard.Color.GREEN);
        UnoCard played = new PickUp2Card(UnoCard.Color.GREEN);
        assertEquals(true,card.canPlay(played));
    }

}