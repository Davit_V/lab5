
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class WildCardTest {
    
    @Test
    public void WildCanPlayTest(){
        NumberedCards card = new NumberedCards(UnoCard.Color.BLUE,NumberedCards.Number.NINE);
        WildCard wild = new WildCard();
        //card  color is  set to null in UnoCard.java
        assertEquals(null, wild.getColor());
        //Playable
        assertEquals(true,wild.canPlay(card));
        //Card Color is set
        assertEquals(UnoCard.Color.BLUE, wild.getColor());
    }

    @Test
    public void WildgetColorTest(){
        WildCard card = new WildCard();
        card.setColor(UnoCard.Color.RED);
        
        assertEquals(UnoCard.Color.RED, card.getColor());
    }
}
