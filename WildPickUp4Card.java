public class WildPickUp4Card extends WildCard {


    /**
     * Since  no other functionality is implemented 
     * in the context of this lab
     * All WildPickUp4Cards are WildCards.
     * Hence the only difference is  between  them
     * is when this card is played the opposing player  picks
     * up 4 cards.
     */
    
}
