public class WildCard implements UnoCard {
    protected Color color;

    public boolean canPlay(UnoCard card){
        if(card.getColor() == Color.BLUE){
            this.color = Color.BLUE;
            return true;
        }else if(card.getColor() == Color.GREEN){
            this.color = Color.GREEN;
            return true;
        }else if(card.getColor() == Color.RED){
            this.color = Color.RED;
            return true;
        }else{
            this.color = Color.YELLOW;
            return true;
        }
    }
    public Color getColor(){
        return this.color;
    }
    /**
     * For jUnit test
     * @return
     */
    public void setColor(Color color){
        this.color = color;
    }
}
