public abstract class ColoredCards implements UnoCard{
    protected Color color;

    public abstract boolean canPlay(UnoCard card);

    public final Color getColor(){
        return this.color;
    }

    public final void setColor(Color color){
        this.color = color;
    }
}